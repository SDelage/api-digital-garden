<?php

namespace DgapiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom');
        $builder->add('prenom');
        $builder->add('alias');
        $builder->add('email');
        $builder->add('plainPassword');
        $builder->add('role');
        $builder->add('dateCreate', FormType::class, array('empty_data' => new \DateTime()));
        $builder->add('dateUpdate', FormType::class, array('empty_data' => new \DateTime()));
        $builder->add('isDelete', CheckboxType::class, array('required' => true));
        $builder->add('isValid', CheckboxType::class, array('required' => true));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'DgapiBundle\Entity\User',
            'csrf_protection' => false
        ]);
    }
}