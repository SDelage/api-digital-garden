<?php

namespace DgapiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;

class CodeAffaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('codeGs');
        $builder->add('codeDg');
        $builder->add('isValid', CheckboxType::class, array('required' => true));
        $builder->add('client');
        $builder->add('dateCreate', FormType::class, array('data' => new \DateTime()));
        $builder->add('dateUpdate', FormType::class, array('data' => new \DateTime()));
        $builder->add('isDelete', CheckboxType::class, array('required' => true));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'DgapiBundle\Entity\CodeAffaire',
            'csrf_protection' => false
        ]);
    }
}