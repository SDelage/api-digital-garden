<?php

namespace DgapiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TacheType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, array('description' => "Nom de la tache"));
        $builder->add('description', null, array('description' => "Description de la tache"));
        $builder->add('comment', null, array('description' => "Commentaire de la tache"));
        $builder->add('dateBrief', null, array('description' => "Date du briefing de la tache"));
        $builder->add('dateLivraisonTest', null, array('description' => "Date de la livraison test de la tache"));
        $builder->add('dateLivraison', null, array('description' => "Date de la livraison final de la tache"));
        $builder->add('time', null, array('description' => "Temps de la tache"));
        $builder->add('realTime', null, array('description' => "Temps Réel de la tache"));
        $builder->add('client', null, array('description' => "Objet client de la tache"));
        $builder->add('codeAffaire', null, array('description' => "Objet code affaire de la tache"));
        $builder->add('cdp', null, array('description' => "Objet chef de projet de la tache"));
        $builder->add('lead', null, array('description' => "Objet lead de la tache"));
        $builder->add('exe', null, array('description' => "Objet executant de la tache"));
        $builder->add('planning', null, array('description' => "Objet planning de la tache"));
        $builder->add('dateCreate', FormType::class, array('data' => new \DateTime()));
        $builder->add('dateUpdate', FormType::class, array('data' => new \DateTime()));
        $builder->add('isDelete', CheckboxType::class, array('required' => true));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'DgapiBundle\Entity\Tache',
            'csrf_protection' => false
        ]);
    }
}