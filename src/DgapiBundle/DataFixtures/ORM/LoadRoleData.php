<?php

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use DgapiBundle\Entity\Role;

class LoadRoleData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /* Admin Role */
        $adminGroup = new Role();
        $adminGroup->setName('Administrateur');
        $adminGroup->setDateCreate(new \DateTime());
        $adminGroup->setDateUpdate(new \DateTime());
        $adminGroup->setIsDelete(0);

        $manager->persist($adminGroup);
        $manager->flush();
        
        $this->addReference('admin-group', $adminGroup);

        /* Other Role */
        $otherRolePlanning = new Role();
        $otherRolePlanning->setName('Planning Manager');
        $otherRolePlanning->setDateCreate(new \DateTime());
        $otherRolePlanning->setDateUpdate(new \DateTime());
        $otherRolePlanning->setIsDelete(0);
        $manager->persist($otherRolePlanning);
        $manager->flush();

        $otherRoleDev = new Role();
        $otherRoleDev->setName('Développeur');
        $otherRoleDev->setDateCreate(new \DateTime());
        $otherRoleDev->setDateUpdate(new \DateTime());
        $otherRoleDev->setIsDelete(0);
        $manager->persist($otherRoleDev);
        $manager->flush();

        $otherRoleCDP = new Role();
        $otherRoleCDP->setName('Chef de projet');
        $otherRoleCDP->setDateCreate(new \DateTime());
        $otherRoleCDP->setDateUpdate(new \DateTime());
        $otherRoleCDP->setIsDelete(0);
        $manager->persist($otherRoleCDP);
        $manager->flush();

    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
}