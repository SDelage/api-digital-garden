<?php

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use DgapiBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setNom('Admin');
        $userAdmin->setPrenom('Master');
        $userAdmin->setAlias('ADM');
        /* Ajouter le mot de passe administrateur */
        $userAdmin->setEmail('admin@digitalgarden.fr');
        $userAdmin->setPlainPassword('4dm1n-m4st3r');
        /* Add reference create role by fixture */
        $userAdmin->setRole($this->getReference('admin-group'));
        $userAdmin->setDateCreate(new \DateTime());
        $userAdmin->setDateUpdate(new \DateTime());
        $userAdmin->setIsValid(1);
        $userAdmin->setIsDelete(0);
        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($userAdmin, $userAdmin->getPlainPassword());
        $userAdmin->setPassword($encoded);

        $manager->persist($userAdmin);
        $manager->flush();

        $this->addReference('admin-user', $userAdmin);
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 2;
    }
}