<?php

namespace DgapiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Tache
 *
 * @ORM\Entity(repositoryClass="DgapiBundle\Repository\TacheRepository")
 */
class Tache
{
    /**
     * Identifiant de la tâche
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Nom de la tâche
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * Description de la tâche
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * Commentaire de la tâche
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * Date de Briefing de la tâche
     * @var string
     *
     * @ORM\Column(name="date_brief", type="string", length=10)
     */
    private $dateBrief;

    /**
     * Date de livraison test de la tâche
     * @var string
     *
     * @ORM\Column(name="date_livraison_test", type="string", length=10)
     */
    private $dateLivraisonTest;

    /**
     * Date de livraison final de la tâche
     * @var string
     *
     * @ORM\Column(name="date_livraison", type="string", length=10)
     */
    private $dateLivraison;

    /**
     * Date de création de la tâche
     * @var \DateTime
     *
     * @ORM\Column(name="date_create", type="datetime")
     */
    private $dateCreate;

    /**
     * Date de mise a jour de la tâche
     * @var \DateTime
     *
     * @ORM\Column(name="date_update", type="datetime")
     */
    private $dateUpdate;

    /**
     * temps a passer sur la tâche
     * @var float
     *
     * @ORM\Column(name="time", type="float")
     */
    private $time;

    /**
     * temps réel passer sur la tâche
     * @var float
     *
     * @ORM\Column(name="real_time", type="float", nullable=true)
     */
    private $realTime;

    /**
     * Variable de suppréssion virtuel de la tâche
     * @var bool
     *
     * @ORM\Column(name="is_delete", type="boolean")
     */
    private $isDelete = false;

    /**
     * Objet Client associer a la tâche
     *
     * @ORM\ManyToOne(targetEntity="DgapiBundle\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * Objet Code Affaire associer a la tâche
     *
     * @ORM\ManyToOne(targetEntity="DgapiBundle\Entity\CodeAffaire")
     * @ORM\JoinColumn(name="code_affaire_id", referencedColumnName="id")
     */
    private $codeAffaire;

    /**
     * Objet Chef de projet associer a la tâche
     *
     * @ORM\ManyToOne(targetEntity="DgapiBundle\Entity\User")
     * @ORM\JoinColumn(name="cdp_id", referencedColumnName="id")
     */
    private $cdp;

    /**
     * Objet Lead associer a la tâche
     *
     * @ORM\ManyToOne(targetEntity="DgapiBundle\Entity\User")
     * @ORM\JoinColumn(name="lead_id", referencedColumnName="id")
     */
    private $lead;

    /**
     * Objet Executant associer a la tâche
     *
     * @ORM\ManyToOne(targetEntity="DgapiBundle\Entity\User")
     * @ORM\JoinColumn(name="exe_id", referencedColumnName="id")
     */
    private $exe;

    /**
     * Objet planning associer a la tâche
     *
     * @ORM\ManyToOne(targetEntity="DgapiBundle\Entity\Planning", inversedBy="taches")
     * @ORM\JoinColumn(name="planning_id", referencedColumnName="id")
     */
    private $planning;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tache
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Tache
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Tache
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set isDelete
     *
     * @param boolean $isDelete
     *
     * @return Tache
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;

        return $this;
    }

    /**
     * Get isDelete
     *
     * @return bool
     */
    public function getIsDelete()
    {
        return $this->isDelete;
    }

    /**
     * Set client
     *
     * @param \DgapiBundle\Entity\Client $client
     *
     * @return Tache
     */
    public function setClient(\DgapiBundle\Entity\Client $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \DgapiBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set codeAffaire
     *
     * @param \DgapiBundle\Entity\CodeAffaire $codeAffaire
     *
     * @return Tache
     */
    public function setCodeAffaire(\DgapiBundle\Entity\CodeAffaire $codeAffaire = null)
    {
        $this->codeAffaire = $codeAffaire;

        return $this;
    }

    /**
     * Get codeAffaire
     *
     * @return \DgapiBundle\Entity\CodeAffaire
     */
    public function getCodeAffaire()
    {
        return $this->codeAffaire;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Tache
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateBrief
     *
     * @param string $dateBrief
     *
     * @return Tache
     */
    public function setDateBrief($dateBrief)
    {
        $this->dateBrief = $dateBrief;

        return $this;
    }

    /**
     * Get dateBrief
     *
     * @return string
     */
    public function getDateBrief()
    {
        return $this->dateBrief;
    }

    /**
     * Set dateLivraisonTest
     *
     * @param string $dateLivraisonTest
     *
     * @return Tache
     */
    public function setDateLivraisonTest($dateLivraisonTest)
    {
        $this->dateLivraisonTest = $dateLivraisonTest;

        return $this;
    }

    /**
     * Get dateLivraisonTest
     *
     * @return string
     */
    public function getDateLivraisonTest()
    {
        return $this->dateLivraisonTest;
    }

    /**
     * Set dateLivraison
     *
     * @param string $dateLivraison
     *
     * @return Tache
     */
    public function setDateLivraison($dateLivraison)
    {
        $this->dateLivraison = $dateLivraison;

        return $this;
    }

    /**
     * Get dateLivraison
     *
     * @return string
     */
    public function getDateLivraison()
    {
        return $this->dateLivraison;
    }

    /**
     * Set cdp
     *
     * @param \DgapiBundle\Entity\User $cdp
     *
     * @return Tache
     */
    public function setCdp(\DgapiBundle\Entity\User $cdp = null)
    {
        $this->cdp = $cdp;

        return $this;
    }

    /**
     * Get cdp
     *
     * @return \DgapiBundle\Entity\User
     */
    public function getCdp()
    {
        return $this->cdp;
    }

    /**
     * Set lead
     *
     * @param \DgapiBundle\Entity\User $lead
     *
     * @return Tache
     */
    public function setLead(\DgapiBundle\Entity\User $lead = null)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * Get lead
     *
     * @return \DgapiBundle\Entity\User
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * Set exe
     *
     * @param \DgapiBundle\Entity\User $exe
     *
     * @return Tache
     */
    public function setExe(\DgapiBundle\Entity\User $exe = null)
    {
        $this->exe = $exe;

        return $this;
    }

    /**
     * Get exe
     *
     * @return \DgapiBundle\Entity\User
     */
    public function getExe()
    {
        return $this->exe;
    }

    /**
     * Set planning
     *
     * @param \DgapiBundle\Entity\Planning $planning
     *
     * @return Tache
     */
    public function setPlanning(\DgapiBundle\Entity\Planning $planning = null)
    {
        $this->planning = $planning;

        return $this;
    }

    /**
     * Get planning
     *
     * @return \DgapiBundle\Entity\Planning
     */
    public function getPlanning()
    {
        return $this->planning;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Tache
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }


    /**
     * Set time
     *
     * @param float $time
     *
     * @return Tache
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return float
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set realTime
     *
     * @param float $realTime
     *
     * @return Tache
     */
    public function setRealTime($realTime)
    {
        $this->realTime = $realTime;

        return $this;
    }

    /**
     * Get realTime
     *
     * @return float
     */
    public function getRealTime()
    {
        return $this->realTime;
    }
}
