<?php

namespace DgapiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * CodeAffaire
 *
 * @ORM\Table(name="code_affaire")
 * @ORM\Entity(repositoryClass="DgapiBundle\Repository\CodeAffaireRepository")
 */
class CodeAffaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="code_gs", type="string", length=255, unique=true, nullable=true)
     */
    private $codeGs;

    /**
     * @var string
     *
     * @ORM\Column(name="code_dg", type="string", length=255, unique=true, nullable=true)
     */
    private $codeDg;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_valid", type="boolean")
     */
    private $isValid = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_create", type="datetime")
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_update", type="datetime")
     */
    private $dateUpdate;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_delete", type="boolean")
     */
    private $isDelete = false;

    /**
     * @ORM\ManyToOne(targetEntity="DgapiBundle\Entity\Client", inversedBy="codeAffaire")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->taches = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeGs
     *
     * @param string $codeGs
     *
     * @return CodeAffaire
     */
    public function setCodeGs($codeGs)
    {
        $this->codeGs = $codeGs;

        return $this;
    }

    /**
     * Get codeGs
     *
     * @return string
     */
    public function getCodeGs()
    {
        return $this->codeGs;
    }

    /**
     * Set codeDg
     *
     * @param string $codeDg
     *
     * @return CodeAffaire
     */
    public function setCodeDg($codeDg)
    {
        $this->codeDg = $codeDg;

        return $this;
    }

    /**
     * Get codeDg
     *
     * @return string
     */
    public function getCodeDg()
    {
        return $this->codeDg;
    }

    /**
     * Set isValid
     *
     * @param boolean $isValid
     *
     * @return CodeAffaire
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * Get isValid
     *
     * @return bool
     */
    public function getIsValid()
    {
        return $this->isValid;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return CodeAffaire
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return CodeAffaire
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set isDelete
     *
     * @param boolean $isDelete
     *
     * @return CodeAffaire
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;

        return $this;
    }

    /**
     * Get isDelete
     *
     * @return bool
     */
    public function getIsDelete()
    {
        return $this->isDelete;
    }

    /**
     * Set client
     *
     * @param \DgapiBundle\Entity\Client $client
     *
     * @return CodeAffaire
     */
    public function setClient(\DgapiBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \DgapiBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
