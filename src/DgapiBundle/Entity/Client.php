<?php

namespace DgapiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Client
 *
 * @ORM\Table(name="client", uniqueConstraints={@ORM\UniqueConstraint(name="client_name_unique",columns={"name"})})
 * @ORM\Entity(repositoryClass="DgapiBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_create", type="datetime")
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_update", type="datetime")
     */
    private $dateUpdate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_delete", type="boolean")
     */
    private $isDelete = false;

    /**
     * @ORM\OneToMany(targetEntity="DgapiBundle\Entity\CodeAffaire", mappedBy="client")
     */
    private $codeAffaire;

    public function __construct()
    {
        $this->taches = new ArrayCollection();
        $this->codeAffaire = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Client
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Client
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }


    /**
     * Set isDelete
     *
     * @param boolean $isDelete
     *
     * @return Client
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;

        return $this;
    }

    /**
     * Get isDelete
     *
     * @return boolean
     */
    public function getIsDelete()
    {
        return $this->isDelete;
    }

    /**
     * Add codeAffaire
     *
     * @param \DgapiBundle\Entity\CodeAffaire $codeAffaire
     *
     * @return Client
     */
    public function addCodeAffaire(\DgapiBundle\Entity\CodeAffaire $codeAffaire)
    {
        $this->codeAffaire[] = $codeAffaire;

        return $this;
    }

    /**
     * Remove codeAffaire
     *
     * @param \DgapiBundle\Entity\CodeAffaire $codeAffaire
     */
    public function removeCodeAffaire(\DgapiBundle\Entity\CodeAffaire $codeAffaire)
    {
        $this->codeAffaire->removeElement($codeAffaire);
    }

    /**
     * Get codeAffaire
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCodeAffaire()
    {
        return $this->codeAffaire;
    }

}
