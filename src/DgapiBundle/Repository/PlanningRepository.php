<?php

namespace DgapiBundle\Repository;

use Doctrine\ORM\Query\ResultSetMapping;

/**
 * PlanningRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PlanningRepository extends \Doctrine\ORM\EntityRepository
{
    public function getLastPlanning()
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('DgapiBundle:Planning', 'p');
        $rsm->addFieldResult('p', 'id', 'id');
        $rsm->addFieldResult('p', 'reference_planning', 'referencePlanning');
        $sql = "SELECT p.id, p.referencePlanning FROM DgapiBundle:Planning p ".
          "ORDER BY p.id DESC";
        $query = $this->_em->createQuery($sql);
        $query->setMaxResults(1);
        $res = $query->getResult();
        return array_shift($res);
    }

    public function alonePlannings()
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('DgapiBundle:Planning', 'p');
        $rsm->addFieldResult('p', 'id', 'id');
        $rsm->addFieldResult('p', 'name', 'name');
        $rsm->addFieldResult('p', 'reference_planning', 'referencePlanning');
        $rsm->addFieldResult('p', 'is_delete', 'isDelete');

        $sql = 'SELECT p.* FROM planning as p WHERE p.is_delete = 0';
        $query = $this->_em->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }

    public function currentPlanning()
    {
        $currentWeek = new \DateTime();
        $formatCurrentWeek = $currentWeek->format('W-Y');

        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('DgapiBundle:Planning', 'p');
        $rsm->addFieldResult('p', 'id', 'id');

        $sql = "SELECT p.id FROM DgapiBundle:Planning p ".
          "WHERE p.referencePlanning = '".$formatCurrentWeek."'";
        $query = $this->_em->createQuery($sql);
        $query->setMaxResults(1);
        $res = $query->getResult();
        $p = array_shift($res);
        return $p['id'];
    }

}
