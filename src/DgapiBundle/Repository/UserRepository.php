<?php

namespace DgapiBundle\Repository;

use Doctrine\ORM\Query\ResultSetMapping;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function getUsersWithRole()
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('DgapiBundle:User', 'u');
        $rsm->addFieldResult('u', 'id', 'id');
        $rsm->addFieldResult('u', 'nom', 'nom');
        $rsm->addFieldResult('u', 'prenom', 'prenom');
        $rsm->addFieldResult('u', 'alias', 'alias');
        $rsm->addFieldResult('u', 'email', 'email');
//        $rsm->addMetaResult('u', 'discr', 'discr'); // discriminator column
//        $rsm->setDiscriminatorColumn('u', 'discr');
//        $rsm->addFieldResult('u', 'role_id', 'role');
        $rsm->addJoinedEntityResult('DgapiBundle:Role' , 'r', 'u', 'role_id');
        $rsm->addFieldResult('r', 'id', 'id');
        $rsm->addFieldResult('r', 'name', 'name');

        $sql = 'SELECT u.id, u.nom, u.prenom, u.alias, u.email, r.id, r.name FROM user as u LEFT JOIN role as r '.
          'ON u.role_id = r.id WHERE u.is_valid = 1 AND u.is_delete = 0 AND r.is_delete = 0 AND u.alias != "ADM"';
        $query = $this->_em->createNativeQuery($sql, $rsm);

        return $query->getResult();
}

    public function aloneUsers()
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('DgapiBundle:User', 'u');
        $rsm->addFieldResult('u', 'id', 'id');
        $rsm->addFieldResult('u', 'alias', 'alias');

        $sql = 'SELECT u.id, u.alias FROM user as u WHERE u.is_valid = 1 AND u.is_delete = 0 AND u.alias != "ADM"';
        $query = $this->_em->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }

    public function getUserModere()
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('DgapiBundle:User', 'u');
        $rsm->addFieldResult('u', 'id', 'id');
        $rsm->addFieldResult('u', 'nom', 'nom');
        $rsm->addFieldResult('u', 'prenom', 'prenom');
        $rsm->addFieldResult('u', 'alias', 'alias');
        $rsm->addFieldResult('u', 'email', 'email');

        $sql = 'SELECT u.id, u.nom, u.prenom, u.alias, u.email FROM user as u WHERE u.is_valid = 0 AND u.alias != "ADM"';
        $query = $this->_em->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }

    public function getAdmin()
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('DgapiBundle:User', 'u');
        $rsm->addFieldResult('u', 'nom', 'nom');
        $rsm->addFieldResult('u', 'prenom', 'prenom');
        $rsm->addFieldResult('u', 'email', 'email');
        $rsm->addFieldResult('u', 'role_id', 'role');
        $sql = "SELECT u.nom, u.prenom, u.email FROM DgapiBundle:User u ".
          "JOIN u.role r WHERE r.name = 'Administrateur'";
        $query = $this->_em->createQuery($sql);
        $query->setMaxResults(1);
        $res = $query->getResult();
        return array_shift($res);
    }

    //    EXAMPLE

    /*

    $rsm = new ResultSetMapping;
    $rsm->addEntityResult('User', 'u');
    $rsm->addFieldResult('u', 'id', 'id');
    $rsm->addFieldResult('u', 'name', 'name');
    $rsm->addJoinedEntityResult('Address' , 'a', 'u', 'address');
    $rsm->addFieldResult('a', 'address_id', 'id');
    $rsm->addFieldResult('a', 'street', 'street');
    $rsm->addFieldResult('a', 'city', 'city');

    $sql = 'SELECT u.id, u.name, a.id AS address_id, a.street, a.city FROM users u ' .
    'INNER JOIN address a ON u.address_id = a.id WHERE u.name = ?';
    $query = $this->_em->createNativeQuery($sql, $rsm);
    $query->setParameter(1, 'romanb');

    $users = $query->getResult();

    */

}
