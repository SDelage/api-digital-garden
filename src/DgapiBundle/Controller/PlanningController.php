<?php

namespace DgapiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\View\View;
use DgapiBundle\Form\Type\PlanningType;
use DgapiBundle\Entity\Planning;

class PlanningController extends Controller
{
    /**
     * @ApiDoc(
     *      description="Récupère la liste des plannings",
     *      output= { "class"= Planning::class, "collection"=true}
     * )
     *
     * @QueryParam(name="aloneplanning", requirements="\d+", default="", description="Retourne uniquement les informations du planning si ce paramètre vaut 1")
     * @param Request $request
     * @return static
     */
    public function getPlanningsAction(Request $request)
    {
        $aloneplanning = $request->get('aloneplanning');
        $currentplanning = $request->get('currentplanning');
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Planning');
        
        if($aloneplanning === "1"){
            $plannings = $repository->alonePlannings();
        }elseif($currentplanning === "1"){
            $planningID = $repository->currentPlanning();
            $plannings = $repository->find($planningID);
        }else{
            $plannings = $repository->findByIsDelete(0);
        }

        $view = View::create($plannings);
        $view->setFormat('json');

        return $view;
    }

    public function getPlanningAction($planning_id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Planning');
        $planning = $repository->find($planning_id);

        if (empty($planning)) {
            throw new NotFoundHttpException('Planning not found');
        }

        $view = View::create($planning);
        $view->setFormat('json');

        return $view;
    }

    public function createPlanningAction(Request $request){
        
        $Planning = new Planning();
        $form = $this->createForm(PlanningType::class, $Planning);
        $form->submit($request->request->all()); // Validation des données

        if($form->isValid()){
            $em =$this->getDoctrine()->getManager();
            $em->persist($Planning);
            $em->flush();
            $view = View::create($Planning, Response::HTTP_CREATED);
        }else{
            $view = View::create($form, Response::HTTP_BAD_REQUEST);
        }
        $view->setFormat('json');
        return $view;

    }

    public function putPlanningAction(Request $request)
    {
        return $this->updatePlanning($request, true);
    }

    public function patchPlanningAction(Request $request)
    {
        return $this->updatePlanning($request, false);
    }

    private function updatePlanning(Request $request, $clearMissing)
    {
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Planning');
        $Planning = $repository->find($request->get('planning_id'));

        if (empty($Planning)) {
            throw new NotFoundHttpException('Code Affaire not found');
        }

        $form = $this->createForm(PlanningType::class, $Planning);

        /* Le paramètre false dit à Symfony de garder les valeurs dans notre
        entité si l'utilisateur n'en fournit pas une dans sa requête */
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) {
            $em =$this->getDoctrine()->getManager();
            $em->persist($Planning);
            $em->flush();
            $view = View::create($Planning, Response::HTTP_OK);
        } else {
            $view = View::create($form, Response::HTTP_BAD_REQUEST);
        }

        $view->setFormat('json');
        return $view;
    }

    /**
     * @ApiDoc(
     *      description="Suppression d'un planning",
     *      statusCodes = {
     *          204 = "Suppréssion du planning avec succès",
     *          400 = "Erreur de suppréssion du planning"
     *      }
     * )
     *
     * @param $planning_id
     * @return static
     */
    public function removePlanningAction($planning_id){

        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Planning');
        $Planning = $repository->find($planning_id);

        if ($Planning) {
            $em =$this->getDoctrine()->getManager();
            $em->remove($Planning);
            $em->flush();
            $view = View::create($Planning, Response::HTTP_NO_CONTENT);
        }else{
            $view = View::create('Erreur de suppression de la ressource', Response::HTTP_BAD_REQUEST);
        }

        $view->setFormat('json');
        return $view;

    }

}
