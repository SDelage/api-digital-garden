<?php

namespace DgapiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\View\View;
use DgapiBundle\Form\Type\RoleType;
use DgapiBundle\Entity\Role;

class RoleController extends Controller
{
    /**
     * @ApiDoc(
     *      description="Récupère la liste des roles",
     *      output= { "class"= Role::class, "collection"=true}
     * )
     * @QueryParam(name="alonerole", requirements="\d+", default="", description="Retourne uniquement les informations du rôle si ce paramètre vaut 1")
     * @param Request $request
     * @return static
     */
    public function getRolesAction(Request $request)
    {
        $alonerole = $request->get('alonerole');
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Role');

        if($alonerole === "1"){
            $roles = $repository->aloneRoles();
        }else{
            $roles = $repository->findAll();
        }

        /* A optimiser */
        foreach ($roles as $k => $role){
            if($role->getName() === "Administrateur"){
                unset($roles[$k]);
            }
        }

        $view = View::create($roles);
        $view->setFormat('json');

        return $view;
    }

    public function getRoleAction($role_id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Role');
        $Role = $repository->find($role_id);

        if (empty($Role)) {
            throw new NotFoundHttpException('Role not found');
        }

        $view = View::create($Role);
        $view->setFormat('json');

        return $view;
    }

    public function createRoleAction(Request $request){

        $Role = new Role();
        $form = $this->createForm(RoleType::class, $Role);

        $form->submit($request->request->all()); // Validation des données

        if($form->isValid()){
            $em =$this->getDoctrine()->getManager();
            $em->persist($Role);
            $em->flush();
            $view = View::create($Role, Response::HTTP_CREATED);
        }else{
            $view = View::create($form, Response::HTTP_BAD_REQUEST);
        }
        $view->setFormat('json');
        return $view;

    }

    public function putRoleAction(Request $request)
    {
        return $this->updateRole($request, true);
    }

    public function patchRoleAction(Request $request)
    {
        return $this->updateRole($request, false);
    }

    private function updateRole(Request $request, $clearMissing)
    {
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Role');
        $Role = $repository->find($request->get('role_id'));

        if (empty($Role)) {
            throw new NotFoundHttpException('Role not found');
        }

        $form = $this->createForm(RoleType::class, $Role);

        /* Le paramètre false dit à Symfony de garder les valeurs dans notre
        entité si l'utilisateur n'en fournit pas une dans sa requête */
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) {
            $em =$this->getDoctrine()->getManager();
            $em->persist($Role);
            $em->flush();
            $view = View::create($Role, Response::HTTP_OK);
        } else {
            $view = View::create($form, Response::HTTP_BAD_REQUEST);
        }

        $view->setFormat('json');
        return $view;
    }

    /**
     * @ApiDoc(
     *      description="Suppression d'un rôle",
     *      statusCodes = {
     *          204 = "Suppréssion du rôle avec succès",
     *          400 = "Erreur de suppréssion du rôle"
     *      }
     * )
     *
     * @param $role_id
     * @return static
     */
    /*public function removeRoleAction($role_id){

        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Role');
        $Role = $repository->find($role_id);

        if ($Role) {
            $em =$this->getDoctrine()->getManager();
            $em->remove($Role);
            $em->flush();
            $view = View::create($Role, Response::HTTP_NO_CONTENT);
        }else{
            $view = View::create('Erreur de suppression de la ressource', Response::HTTP_BAD_REQUEST);
        }

        $view->setFormat('json');
        return $view;

    }*/

}
