<?php

namespace DgapiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View;
use DgapiBundle\Form\Type\CodeAffaireType;
use DgapiBundle\Entity\CodeAffaire;
use DgapiBundle\Entity\History;

class CodeAffaireController extends Controller
{
    /**
     * @ApiDoc(
     *      description="Récupère la liste des codes affaires",
     *      output= { "class"= CodeAffaire::class, "collection"=true}
     * )
     * @param Request $request
     * @return static
     */
    public function getCodeAffairesAction(Request $request)
    {
        $client_id = $request->get('client_id');

        $em = $this->get('doctrine.orm.entity_manager')->createQueryBuilder();
        $em->select('ca');
        $em->from('DgapiBundle:CodeAffaire', 'ca');

        if(!empty($client_id)){
            $em->where('ca.client = :client AND ca.isDelete = :isDelete')
              ->setParameter('client', $client_id)
              ->setParameter('isDelete', 0);
        }

        $CodeAffaires = $em->getQuery()->getResult();

        $view = View::create($CodeAffaires);
        $view->setFormat('json');

        return $view;
    }

    public function getCodeAffaireAction($code_affaire_id)
    {
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:CodeAffaire');
        $CodeAffaire = $repository->find($code_affaire_id);

        if (empty($CodeAffaire)) {
            throw new NotFoundHttpException('Code Affaire not found');
        }

        $view = View::create($CodeAffaire);
        $view->setFormat('json');

        return $view;
    }

    public function createCodeAffaireAction(Request $request){

        $codeAffaire = new CodeAffaire();
        $form = $this->createForm(CodeAffaireType::class, $codeAffaire);
        $form->submit($request->request->all()); // Validation des données

        if($form->isValid()){
            $em =$this->getDoctrine()->getManager();
            $em->persist($codeAffaire);
            $em->flush();

            /* Add Action to history */
            $this->createHistoryAction('Création du Code Affaire '.$codeAffaire->getCodeGs().' / '.$codeAffaire->getCodeDg().
              ' pour le client '.$codeAffaire->getClient()->getName(),$request->headers->all()['x-auth-token'][0]);

            $view = View::create($codeAffaire, Response::HTTP_CREATED);
        }else{
            $view = View::create($form, Response::HTTP_BAD_REQUEST);
        }
        $view->setFormat('json');
        return $view;

    }

    public function putCodeAffaireAction(Request $request)
    {
        return $this->updateCodeAffaire($request, true);
    }

    public function patchCodeAffaireAction(Request $request)
    {
        return $this->updateCodeAffaire($request, false);
    }

    private function updateCodeAffaire(Request $request, $clearMissing)
    {
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:CodeAffaire');
        $codeAffaire = $repository->find($request->get('code_affaire_id'));

        if (empty($codeAffaire)) {
            throw new NotFoundHttpException('Code Affaire not found');
        }

        $form = $this->createForm(CodeAffaireType::class, $codeAffaire);

        /* Le paramètre false dit à Symfony de garder les valeurs dans notre
        entité si l'utilisateur n'en fournit pas une dans sa requête */
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) {
            $em =$this->getDoctrine()->getManager();
            $em->persist($codeAffaire);
            $em->flush();

            /* Add Action to history */
            if($codeAffaire->getIsDelete()){
                $this->createHistoryAction('Suppression du Code Affaire '.$codeAffaire->getCodeGs().' / '.$codeAffaire->getCodeDg().
                  ' pour le client '.$codeAffaire->getClient()->getName(),$request->headers->all()['x-auth-token'][0]);
            }else{
                $this->createHistoryAction('Modification du Code Affaire '.$codeAffaire->getCodeGs().' / '.$codeAffaire->getCodeDg().
                  ' pour le client '.$codeAffaire->getClient()->getName(),$request->headers->all()['x-auth-token'][0]);
            }

            $view = View::create($codeAffaire, Response::HTTP_OK);
        } else {
            $view = View::create($form, Response::HTTP_BAD_REQUEST);
        }

        $view->setFormat('json');
        return $view;
    }

    /**
     * @ApiDoc(
     *      description="Suppression d'un code affaire",
     *      statusCodes = {
     *          204 = "Suppréssion du code affaire avec succès",
     *          400 = "Erreur de suppréssion du code affaire"
     *      }
     * )
     *
     * @param Request $request
     * @return static
     */
    public function removeCodeAffaireAction(Request $request){

        $repository = $this->getDoctrine()->getRepository('DgapiBundle:CodeAffaire');
        $codeAffaire = $repository->find($request->get('code_affaire_id'));

        if ($codeAffaire) {
            $em =$this->getDoctrine()->getManager();
            $em->remove($codeAffaire);
            $em->flush();

            /* Add Action to history */
            $this->createHistoryAction('Suppression du Code Affaire '.$codeAffaire->getCodeGs().' / '.$codeAffaire->getCodeDg().
              ' pour le client '.$codeAffaire->getClient()->getName(),$request->headers->all()['x-auth-token'][0]);

            $view = View::create($codeAffaire, Response::HTTP_NO_CONTENT);
        }else{
            $view = View::create('Erreur de suppression de la ressource', Response::HTTP_BAD_REQUEST);
        }

        $view->setFormat('json');
        return $view;

    }

    /* Solution Tmp le temps de trouver comment etendre les fonction du controller History  */
    public function createHistoryAction($description, $tok_user)
    {
        $history = new History();
        $repository_auth = $this->getDoctrine()->getRepository('DgapiBundle:AuthToken');

        $auth = $repository_auth->findOneByValue($tok_user);
        $history->setDescription($description);
        $history->setUser($auth->getUser());
        $history->setDateCreate(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($history);
        $em->flush();
    }

}
