<?php

namespace DgapiBundle\Controller;

use DgapiBundle\Entity\History;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View;

class HistoryController extends Controller
{
    /**
     * @ApiDoc(
     *      description="Récupère la liste de l'historique",
     *      output= { "class"= History::class, "collection"=true}
     * )
     * @return static
     */
    public function getHistoryAction()
    {
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:History');
        $history = $repository->findAll();
        
        $view = View::create($history);
        $view->setFormat('json');

        return $view;
    }
}
