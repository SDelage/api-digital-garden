<?php

namespace DgapiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\View\View;
use DgapiBundle\Form\Type\UserType;
use DgapiBundle\Entity\User;
use DgapiBundle\Entity\History;

class UserController extends Controller
{
    /**
     * @ApiDoc(
     *      description="Récupère la liste des utilisateurs",
     *      output= { "class"= User::class, "collection"=true}
     * )
     * @QueryParam(name="aloneuser", requirements="\d+", default="", description="Retourne uniquement les informations de l'utilisateur si ce paramètre vaut 1")
     * @param Request $request
     * @return static
     */
    public function getUsersAction(Request $request)
    {
        $aloneuser = $request->get('aloneuser');
        $ismodere = $request->get('ismodere');

        $repository = $this->getDoctrine()->getRepository('DgapiBundle:User');

        if((int)$aloneuser === 1){
            $users = $repository->aloneUsers();
        }elseif((int)$ismodere === 1){
            $users = $repository->getUserModere();
        }else{
            $users = $repository->findBy(['isValid' => 1, 'isDelete' => 0]);
        }

        /* A optimiser */
        foreach ($users as $k => $user){
            if(!empty($user->getRole()) || $user->getRole() !== null){
                if($user->getRole()->getName() === "Administrateur"){
                    unset($users[$k]);
                }
            }
        }

        $view = View::create($users);
        $view->setFormat('json');

        return $view;
    }

    public function getUserAction($user_id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:User');
        $User = $repository->find($user_id);

        if (empty($User)) {
            //return new JsonResponse(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
            throw new NotFoundHttpException('User not found');
        }

        $view = View::create($User);
        $view->setFormat('json');

        return $view;
    }

    public function createUserAction(Request $request){

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        
        $user_request = $request->request->all();

        $form->submit($user_request); // Validation des données

        if($form->isValid()){
            $encoder = $this->get('security.password_encoder');
            // le mot de passe en claire est encodé avant la sauvegarde
            $encoded = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($encoded);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $view = View::create($user, Response::HTTP_CREATED);
        }else{
            $view = View::create($form, Response::HTTP_BAD_REQUEST);
        }
        $view->setFormat('json');
        return $view;

    }

    public function putUserAction(Request $request)
    {
        return $this->updateUser($request, true);
    }

    public function patchUserAction(Request $request)
    {
        return $this->updateUser($request, false);
    }

    private function updateUser(Request $request, $clearMissing)
    {
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:User');
        $isModere = $request->get('ismodere');
        $user = $repository->find($request->get('user_id'));

        if (empty($user)) {
            //return new JsonResponse(['message' => 'Place not found'], Response::HTTP_NOT_FOUND);
            throw new NotFoundHttpException('User not found');
        }
        if((int)$isModere === 1){
            /* Test si l'utilisateur modere a bien un role */
            if($request->get('role') === null){
                $view = View::create('Erreur aucun role défini pour l\'utilisateur modere' , Response::HTTP_BAD_REQUEST);
                $view->setFormat('json');
                return $view;
            }

        }

        $form = $this->createForm(UserType::class, $user);
        /* Le paramètre false dit à Symfony de garder les valeurs dans notre
        entité si l'utilisateur n'en fournit pas une dans sa requête */
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) {
            if($isModere === "1"){
                $this->sendMailToModereUser($user);
                /* Add Action to history */
                $this->createHistoryAction("Création de l'utilisateur ".$user->getNom().' '.$user->getPrenom(),
                  $request->headers->all()['x-auth-token'][0]);
            }elseif($user->getIsDelete()){
                /* Add Action to history */
                $this->createHistoryAction("Suppression de l'utilisateur ".$user->getNom().' '.$user->getPrenom(),
                  $request->headers->all()['x-auth-token'][0]);
            }else{
                /* Add Action to history */
                $this->createHistoryAction("Modification de l'utilisateur ".$user->getNom().' '.$user->getPrenom(),
                  $request->headers->all()['x-auth-token'][0]);
            }
            $em =$this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $view = View::create($user, Response::HTTP_OK);
        } else {
            $view = View::create($form, Response::HTTP_BAD_REQUEST);
        }

        $view->setFormat('json');
        return $view;
    }

    /**
     * @ApiDoc(
     *      description="Suppression d'un utilisateur",
     *      statusCodes = {
     *          204 = "Suppréssion de l'utilisateur avec succès",
     *          400 = "Erreur de suppréssion de l'utilisateur"
     *      }
     * )
     *
     * @param Request $request
     * @return static
     */
    public function removeUserAction(Request $request){

        $repository = $this->getDoctrine()->getRepository('DgapiBundle:User');
        $user = $repository->find($request->get('user_id'));

        if ($user) {
            $em =$this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            /* Add Action to history */
            $this->createHistoryAction("Suppression de l'utilisateur ".$user->getNom().' '.$user->getPrenom(),
              $request->headers->all()['x-auth-token'][0]);

            $view = View::create($user, Response::HTTP_NO_CONTENT);
        }else{
            $view = View::create('Erreur de suppression de la ressource', Response::HTTP_BAD_REQUEST);
        }

        $view->setFormat('json');
        return $view;

    }

    /**
     * @param $user
     */
    private function sendMailToModereUser(\DgapiBundle\Entity\User $user)
    {

        $repository = $this->getDoctrine()->getRepository('DgapiBundle:User');
        $admin = $repository->getAdmin();

        if (!empty($admin)) {
            $message = new \Swift_Message('Bienvenue');
            $message->setFrom($admin['email'], $admin['nom'].' '.$admin['prenom'])
              ->setTo($user->getEmail())
              ->setBody($this->renderView('DgapiBundle:Email:register.html.twig', array('user' => $user)), 'text/html');

            $this->get('mailer')->send($message);
        }

    }

    /* Solution Tmp le temps de trouver comment etendre les fonction du controller History  */
    public function createHistoryAction($description, $tok_user)
    {
        $history = new History();
        $repository_auth = $this->getDoctrine()->getRepository('DgapiBundle:AuthToken');

        $auth = $repository_auth->findOneByValue($tok_user);
        $history->setDescription($description);
        $history->setUser($auth->getUser());
        $history->setDateCreate(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($history);
        $em->flush();
    }

}
