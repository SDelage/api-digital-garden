<?php

namespace DgapiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\View\View;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $routes = $this->get('router')->getRouteCollection()->all();

        $view = View::create($routes);
        $view->setFormat('json');

        return $view;
    }
}
