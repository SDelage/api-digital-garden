<?php

namespace DgapiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\View\View;
use DgapiBundle\Form\Type\TacheType;
use DgapiBundle\Entity\Tache;
use DgapiBundle\Entity\History;

class TacheController extends Controller
{
    /**
     * @ApiDoc(
     *      description="Récupère la liste des tâches",
     *      output= { "class"= Tache::class, "collection"=true}
     * )
     * @QueryParam(name="planning_id", requirements="\d+", default="",
     *     description="Retourne uniquement les taches appartenant a ce planning")
     * @param Request $request
     * @return static
     */
    public function getTachesAction(Request $request)
    {
        $planning_id = $request->get('planning_id');

        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Tache');

        if(!empty($planning_id)){
            $taches = $repository->findByPlanning($planning_id);
        }else{
            $taches = $repository->findAll();
        }

        $view = View::create($taches);
        $view->setFormat('json');

        return $view;
    }

    /**
     * @ApiDoc(
     *      description="Récupère les informations d'une tâche",
     *      output= { "class"= Tache::class, "collection"=false}
     * )
     * @param $tache_id
     * @param Request $request
     * @return static
     */
    public function getTacheAction($tache_id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Tache');
        $tache = $repository->find($tache_id);

        if (empty($tache)) {
            throw new NotFoundHttpException('Tache not found');
        }

        $view = View::create($tache);
        $view->setFormat('json');

        return $view;
    }

    /**
     * @ApiDoc(
     *      description="Création d'une nouvelle tâche",
     *      input={"class"=Tache::class, "name"=""},
     *      statusCodes = {
     *          201 = "Création avec succès",
     *          400 = "Formulaire invalide"
     *      },
     *      responseMap={
     *          201 = {"class"=Tache::class},
     *          400 = { "class"=TacheType::class, "form_errors"=true, "name" = ""}
     *      }
     * )
     * @param Request $request
     * @return static
     */
    public function createTacheAction(Request $request){

        $tache = new Tache();
        $form = $this->createForm(TacheType::class, $tache);

        $newTache = $this->formatDateForTache($request->request->all());

        $form->submit($newTache); // Validation des données

        if($form->isValid()){
            $em =$this->getDoctrine()->getManager();
            $em->persist($tache);
            $em->flush();

            /* Add Action to history */
            $this->createHistoryAction('Création de la tache '.$tache->getName().' de la Semaine '.
              $tache->getPlanning()->getReferencePlanning(),$request->headers->all()['x-auth-token'][0]);

            $view = View::create($tache, Response::HTTP_CREATED);
        }else{
            $view = View::create($form, Response::HTTP_BAD_REQUEST);
        }
        $view->setFormat('json');
        return $view;
    }

    /**
     * @ApiDoc(
     *      description="Mise a jours compléte d'une tache",
     *      output= { "class"= Tache::class, "collection"=false},
     *      statusCodes = {
     *          200 = "Modification complète avec succès",
     *          400 = "Formulaire invalide"
     *      },
     *      responseMap={
     *          200 = {"class"=Tache::class},
     *          400 = {"class"=TacheType::class}
     *      }
     * )
     *
     * @param Request $request
     * @return static
     */
    public function putTacheAction(Request $request)
    {
        return $this->updateTache($request, true);
    }

    /**
     * @ApiDoc(
     *      description="Mise a jours partielle d'une tache",
     *      output= { "class"= Tache::class, "collection"=false},
     *      statusCodes = {
     *          200 = "Modification partielle avec succès",
     *          400 = "Formulaire invalide"
     *      },
     *      responseMap={
     *          200 = {"class"=Tache::class},
     *          400 = {"class"=TacheType::class}
     *      }
     * )
     *
     * @param Request $request
     * @return static
     */
    public function patchTacheAction(Request $request)
    {
        return $this->updateTache($request, false);
    }

    private function updateTache(Request $request, $clearMissing)
    {
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Tache');
        $tache = $repository->find($request->get('tache_id'));

        if (empty($Tache)) {
            throw new NotFoundHttpException('Tache not found');
        }

        $form = $this->createForm(TacheType::class, $tache);

        $TacheParam = $this->formatDateForTache($request->request->all());
        /* Le paramètre false dit à Symfony de garder les valeurs dans notre
        entité si l'utilisateur n'en fournit pas une dans sa requête */
        $form->submit($TacheParam, $clearMissing);

        if ($form->isValid()) {
            $em =$this->getDoctrine()->getManager();
            $em->persist($tache);
            $em->flush();

            /* Add Action to history */
            if($tache->getIsDelete()){
                $this->createHistoryAction('Suppression de la tache '.$tache->getName().' de la Semaine '.
                  $tache->getPlanning()->getReferencePlanning(),$request->headers->all()['x-auth-token'][0]);
            }else{
                $this->createHistoryAction('Modification de la tache '.$tache->getName().' de la Semaine '.
                  $tache->getPlanning()->getReferencePlanning(),$request->headers->all()['x-auth-token'][0]);
            }

            $view = View::create($tache, Response::HTTP_OK);
        } else {
            $view = View::create($form, Response::HTTP_BAD_REQUEST);
        }

        $view->setFormat('json');
        return $view;
    }

    /**
     * @ApiDoc(
     *      description="Suppression d'une tache",
     *      statusCodes = {
     *          204 = "Suppréssion de la tâche avec succès",
     *          400 = "Erreur de suppréssion de la tâche"
     *      }
     * )
     *
     * @param Request $request
     * @return static
     */
    public function removeTacheAction(Request $request){

        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Tache');
        $tache = $repository->find($request->get('tache_id'));

        if ($tache) {
            $em =$this->getDoctrine()->getManager();
            $em->remove($tache);
            $em->flush();

            /* Add Action to history */
            $this->createHistoryAction('Suppression de la tache '.$tache->getName().' de la Semaine '.
              $tache->getPlanning()->getReferencePlanning(),$request->headers->all()['x-auth-token'][0]);

            $view = View::create($tache, Response::HTTP_NO_CONTENT);
        }else{
            $view = View::create('Erreur de suppression de la ressource', Response::HTTP_BAD_REQUEST);
        }

        $view->setFormat('json');
        return $view;

    }

    /* Format date */
    private function formatDateForTache($objectTache){
        if(isset($objectTache['dateBrief']) && !empty($objectTache['dateBrief'])){
            $objectTache['dateBrief'] = new \DateTime($objectTache['dateBrief']);
            //$objectTache['dateBrief'] = $objectTache['dateBrief']->modify('+1 day')->format('d/m/Y');
            $objectTache['dateBrief'] = $objectTache['dateBrief']->format('d/m/Y');
        }
        if(isset($objectTache['dateBrief']) && !empty($objectTache['dateBrief'])){
            $objectTache['dateLivraisonTest'] = new \DateTime($objectTache['dateLivraisonTest']);
            //$objectTache['dateLivraisonTest'] = $objectTache['dateLivraisonTest']->modify('+1 day')->format('d/m/Y');
            $objectTache['dateLivraisonTest'] = $objectTache['dateLivraisonTest']->format('d/m/Y');
        }

        if(isset($objectTache['dateBrief']) && !empty($objectTache['dateBrief'])){
            $objectTache['dateLivraison'] = new \DateTime($objectTache['dateLivraison']);
            //$objectTache['dateLivraison'] = $objectTache['dateLivraison']->modify('+1 day')->format('d/m/Y');
            $objectTache['dateLivraison'] = $objectTache['dateLivraison']->format('d/m/Y');
        }
        return $objectTache;
    }

    /* Solution Tmp le temps de trouver comment etendre les fonction du controller History  */
    public function createHistoryAction($description, $tok_user)
    {
        $history = new History();
        $repository_auth = $this->getDoctrine()->getRepository('DgapiBundle:AuthToken');

        $auth = $repository_auth->findOneByValue($tok_user);
        $history->setDescription($description);
        $history->setUser($auth->getUser());
        $history->setDateCreate(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($history);
        $em->flush();
    }
}
