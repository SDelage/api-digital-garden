<?php

namespace DgapiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\View\View;
use DgapiBundle\Form\Type\ClientType;
use DgapiBundle\Entity\Client;
use DgapiBundle\Entity\History;

/**
 * Class ClientController
 * Controller pour l'objet Client
 *
 * @package DgapiBundle\Controller
 * @author  Delage Sylvain
 */
class ClientController extends Controller
{
    /**
     * @ApiDoc(
     *      description="Récupère la liste des clients",
     *      output= { "class"=Client::class, "collection"=true}
     * )
     * @QueryParam(name="aloneclient", requirements="\d+", default="0", description="Retourne uniquement les informations du client si ce paramètre vaut 1")
     * @link /clients
     * @return static
     */
    public function getClientsAction(Request $request)
    {
        $aloneclient = $request->get('aloneclient');
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Client');

        if($aloneclient === "1"){
            $clients = $repository->aloneClients();
        }else{
            $clients = $repository->findByIsDelete(0);
            /* A optimiser */
            foreach ($clients as $c){
                foreach ($c->getCodeAffaire() as $k => $ca){
                    if($ca->getIsDelete()){
                        unset($c->getCodeAffaire()[$k]);
                    }
                }
            }
        }
        
        $view = View::create($clients);
        $view->setFormat('json');

        return $view;
    }

    /**
     * @ApiDoc(
     *      description="Récupère les informations d'un clients",
     *      output= { "class"=Client::class, "collection"=false}
     * )
     *
     * @link /clients/$client_id
     * @param $client_id
     * @return static
     */
    public function getClientAction($client_id)
    {
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Client');
        $client = $repository->find($client_id);

        if (empty($client)) {
            throw new NotFoundHttpException('Client not found');
        }

        $view = View::create($client);
        $view->setFormat('json');

        return $view;
    }

    /**
     * @ApiDoc(
     *      description="Création d'un nouveau client",
     *      input={"class"=Client::class, "name"=""}
     * )
     *
     * @link /clients
     * @param Request $request
     * @return static
     */
    public function createClientAction(Request $request){

        $client = new Client();
        $form = $this->createForm(ClientType::class, $client);
        $form->submit($request->request->all()); // Validation des données

        if($form->isValid()){
            $em =$this->getDoctrine()->getManager();
            $em->persist($client);
            $em->flush();

            /* Add Action to history */
            $this->createHistoryAction("Création du client ".$client->getName(),$request->headers->all()['x-auth-token'][0]);

            $view = View::create($client, Response::HTTP_CREATED);
        }else{
            $view = View::create($form, Response::HTTP_BAD_REQUEST);
        }
        $view->setFormat('json');
        return $view;

    }

    /**
     * @ApiDoc(
     *      description="Mise a jours compléte d'un client"
     * )
     *
     * @link /clients
     * @param Request $request
     * @return static
     */
    public function putClientAction(Request $request)
    {
        return $this->updateClient($request, true);
    }

    /**
     * @ApiDoc(
     *      description="Mise a jours partielle d'un client"
     * )
     *
     * @link /clients
     * @param Request $request
     * @return static
     */
    public function patchClientAction(Request $request)
    {
        return $this->updateClient($request, false);
    }

    /**
     * Fontion de mise a jours d'un client selon si elle est compléte ou partielle
     * @param Request $request
     * @param $clearMissing
     * @return static
     */
    private function updateClient(Request $request, $clearMissing)
    {
        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Client');
        $client = $repository->find($request->get('client_id'));

        if (empty($client)) {
            throw new NotFoundHttpException('Client not found');
        }

        $form = $this->createForm(ClientType::class, $client);

        /* Update DateUpdate */
        $requestclient = $request->request->all();
        /*$dateUpdate = new \DateTime();
        $requestclient["dateUpdate"] = $dateUpdate->format('Y-m-d h:m:s');*/

        /* Le paramètre false dit à Symfony de garder les valeurs dans notre
        entité si l'utilisateur n'en fournit pas une dans sa requête */
        $form->submit($requestclient, $clearMissing);

        if ($form->isValid()) {
            $em =$this->getDoctrine()->getManager();
            $em->persist($client);
            $em->flush();

            /* Add Action to history */
            if($client->getIsDelete()){
                // Si suppression virtuel
                $this->createHistoryAction("Suppression du client ".$client->getName(),$request->headers->all()['x-auth-token'][0]);
            }else{
                $this->createHistoryAction("Modification du client ".$client->getName(),$request->headers->all()['x-auth-token'][0]);
            }


            $view = View::create($client, Response::HTTP_OK);
        } else {
            $view = View::create($form, Response::HTTP_BAD_REQUEST);
        }

        $view->setFormat('json');
        return $view;
    }

    /**
     * @ApiDoc(
     *      description="Suppression d'un client",
     *      statusCodes = {
     *          204 = "Suppréssion du client avec succès",
     *          400 = "Erreur de suppréssion du client"
     *      }
     * )
     *
     * @link /clients/$client_id
     * @param Request $request
     * @return static
     */
    public function removeClientAction(Request $request){

        $repository = $this->getDoctrine()->getRepository('DgapiBundle:Client');
        $client = $repository->find($request->get('client_id'));

        if ($client) {
            $em =$this->getDoctrine()->getManager();
            $em->remove($client);
            $em->flush();

            /* Add Action to history */
            $this->createHistoryAction("Suppression du client ".$client->getName(),$request->headers->all()['x-auth-token'][0]);

            $view = View::create($client, Response::HTTP_NO_CONTENT);
        }else{
            $view = View::create('Erreur de suppression de la ressource', Response::HTTP_BAD_REQUEST);
        }

        $view->setFormat('json');
        return $view;

    }

    /* Solution Tmp le temps de trouver comment etendre les fonction du controller History  */
    public function createHistoryAction($description, $tok_user)
    {
        $history = new History();
        $repository_auth = $this->getDoctrine()->getRepository('DgapiBundle:AuthToken');

        $auth = $repository_auth->findOneByValue($tok_user);
        $history->setDescription($description);
        $history->setUser($auth->getUser());
        $history->setDateCreate(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($history);
        $em->flush();
    }

}
