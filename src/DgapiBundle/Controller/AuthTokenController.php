<?php
namespace DgapiBundle\Controller;

use DgapiBundle\DgapiBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use DgapiBundle\Form\Type\CredentialsType;
use DgapiBundle\Entity\AuthToken;
use DgapiBundle\Entity\Credentials;
use FOS\RestBundle\View\View;

class AuthTokenController extends Controller
{
    public function postAuthTokensAction(Request $request)
    {
        $credentials = new Credentials();
        $form = $this->createForm(CredentialsType::class, $credentials);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('DgapiBundle:User')->findOneByEmail($credentials->getLogin());

        if (!$user) { // L'utilisateur n'existe pas
            return $this->invalidCredentials();
        }

        $encoder = $this->get('security.password_encoder');
        $isPasswordValid = $encoder->isPasswordValid($user, $credentials->getPassword());

        if ((int)$user->getIsValid() === 0) { // Le compte n'est pas modere
            return $this->invalidModere();
        }
        if ((int)$user->getIsDelete() === 1) { // Le compte est supprimer
            return $this->invalidUser();
        }
        if (!$isPasswordValid) { // Le mot de passe n'est pas correct
            return $this->invalidCredentials();
        }

        $authToken = new AuthToken();
        $authToken->setValue(base64_encode(random_bytes(50)));
        $authToken->setCreatedAt(new \DateTime('now'));
        $authToken->setUser($user);

        $em->persist($authToken);
        $em->flush();

        $view = View::create($authToken, Response::HTTP_CREATED);
        $view->setFormat('json');
        return $view;
    }

    /**
     * @ApiDoc(
     *      description="Suppression d'un token",
     *      statusCodes = {
     *          204 = "Suppréssion du token avec succès",
     *          400 = "Erreur de suppréssion du token"
     *      }
     * )
     *
     * @param Request $request
     * @return static
     */
    public function removeAuthTokenAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $authToken = $em->getRepository('DgapiBundle:AuthToken')->find($request->get('token_id'));

        $connectedUser = $this->get('security.token_storage')->getToken()->getUser();

        if ($authToken && $authToken->getUser()->getId() === $connectedUser->getId()) {
            $em->remove($authToken);
            $em->flush();

            $view = View::create(['message' => 'Déconnexion'], Response::HTTP_NO_CONTENT);
            $view->setFormat('json');
            return $view;
        } else {
            throw new BadRequestHttpException();
        }
    }

    public function AuthTestAction()
    {
        $view = View::create("Authentification accepté");
        $view->setFormat('json');

        return $view;
    }

    private function invalidCredentials()
    {
        throw new BadRequestHttpException('Invalid credentials');
    }

    private function invalidUser()
    {
        throw new BadRequestHttpException('Compte utilisateur supprimé');
    }
    
    private function invalidModere()
    {
        throw new BadRequestHttpException('Invalid Modere');
    }
}