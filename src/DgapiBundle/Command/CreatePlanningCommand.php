<?php

namespace DgapiBundle\Command;

use DgapiBundle\Entity\Planning;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreatePlanningCommand extends ContainerAwareCommand {

    protected function configure () {
        // On set le nom de la commande
        $this->setName('cron:generatePlanning');
        // On set la description
        $this->setDescription("Permet de creer les plannings des X semaines suivante");
        // On set l'aide
        $this->setHelp("Param : nombre de semaine a créer");

        // On prépare les arguments
        $this->addArgument('nbSemaine', InputArgument::REQUIRED, "Nombre de semaine a créer ?");
    }

    public function execute (InputInterface $input, OutputInterface $output) {

        $nbSemaine = $input->getArgument('nbSemaine');
        $repository = $this->getContainer()->get('doctrine')->getRepository('DgapiBundle:Planning');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $planningText = [];
        $lastPlanning = $repository->getLastPlanning();
        $currentWeek = new \DateTime();

        if(!empty($lastPlanning)){
            $date = explode('-',$lastPlanning['referencePlanning']);
            $currentWeek->setISODate($date[1],$date[0]);
        }
        for ($ii = 0; $ii < $nbSemaine; $ii++){
            $currentWeek->modify('+1 week');
            $newPlanning = new Planning();
            $newPlanning->setName('Semaine '.$currentWeek->format('W'). ' - '.$currentWeek->format('Y'));
            $newPlanning->setReferencePlanning($currentWeek->format('W-Y'));
            $newPlanning->setDateCreate(new \DateTime());
            $newPlanning->setDateUpdate(new \DateTime());
            $newPlanning->setIsDelete(0);
            $planningText[] = $newPlanning->getReferencePlanning();
            $em->persist($newPlanning);
            $em->flush();
        }

        $outputText = $planningText;
        $output->writeln($outputText);
    }
}